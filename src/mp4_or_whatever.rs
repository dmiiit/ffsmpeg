//! "formally, ISO/IEC 14496-14:2003" — Wikipedia, circa 2021

use std::{
    error::Error,
    fmt::Display,
    io::{Read, Seek},
};

pub struct Meta {
    pub magic: u32,
}

impl Meta {
    pub fn parse(buf: &mut (impl Read + Seek)) -> Result<Self, Dangit> {
        let mut magic = [0u8; 4];
        buf.read_exact(&mut magic)?;
        let magic = u32::from_be_bytes(magic);
        let meta = Meta { magic };
        Ok(meta)
    }
}

#[derive(Debug)]
pub enum Dangit {
    CantRead,
}

impl Display for Dangit {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result { todo!() }
}

impl Error for Dangit {}

impl From<std::io::Error> for Dangit {
    fn from(_: std::io::Error) -> Self { Dangit::CantRead }
}
