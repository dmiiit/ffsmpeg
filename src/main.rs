#![feature(duration_checked_float)]

mod mp4_or_whatever;

use std::{error::Error, fmt::Display, fs::File, path::PathBuf, str::FromStr, time::Duration};

use argh::FromArgs;

use crate::mp4_or_whatever::{Dangit, Meta};

fn main() {
    // Ok, game plan:
    // 1) sanity check magic bytes to ensure we're dealing with an MP4 container
    // 1b) or MKV; wait, are those different formats?
    // 2) get framerate from video metadata
    // 3) oh dang, we're gonna have to deal with audio as well, huh
    // 3b) (has this joke gone too far?)

    let args: Args = argh::from_env();

    if args.end.is_some() && args.duration.is_some() {
        eprintln!(
            "You can't specify both the ending timestamp *and* the clip duration! Think, Mark, think!"
        );
        std::process::exit(69);
    }

    if let Err(err) = do_the_thing(args) {
        eprintln!("We have failed, master: {}", err);
        std::process::exit(420);
    }
}

#[derive(FromArgs)]
#[argh(description = "Cut a clip out of an MP4-encoded video file.")]
struct Args {
    #[argh(positional, description = "input file")]
    input_file: PathBuf,
    #[argh(
        option,
        short = 'o',
        description = "output filename",
        default = "\"output.mp4\".into()"
    )]
    output_file: PathBuf,
    #[argh(
        option,
        short = 's',
        description = "starting timestamp",
        from_str_fn(parse_duration),
        default = "Duration::ZERO"
    )]
    start: Duration,
    #[argh(option, short = 'e', description = "ending timestamp", from_str_fn(parse_duration))]
    end: Option<Duration>,
    #[argh(option, short = 'd', description = "clip length", from_str_fn(parse_duration))]
    duration: Option<Duration>,
    #[argh(
        option,
        short = 'b',
        description = "snap to nearest keyframe?",
        default = "CutBounds::Exact"
    )]
    bounds: CutBounds,
    #[argh(
        option,
        short = 'a',
        description = "don't include audio in the clip",
        default = "false"
    )]
    ignore_audio: bool,
}

enum CutBounds {
    Exact,
    PrevKeyFrame,
    NextKeyFrame,
}

impl FromStr for CutBounds {
    type Err = Oopsie;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s.eq_ignore_ascii_case("prev") {
            Ok(CutBounds::PrevKeyFrame)
        } else if s.eq_ignore_ascii_case("next") {
            Ok(CutBounds::NextKeyFrame)
        } else if s.eq_ignore_ascii_case("exact") {
            Ok(CutBounds::Exact)
        } else {
            Err(Oopsie::BadBounds)
        }
    }
}

fn parse_duration(str: &str) -> Result<Duration, String> {
    let hms = str.splitn(3, ':').collect::<Vec<_>>();
    match hms[..] {
        [seconds] => {
            let seconds = seconds.parse().map_err(|_| Oopsie::BadDuration.to_string())?;
            let duration = Duration::try_from_secs_f64(seconds)
                .map_err(|_| Oopsie::BadDuration.to_string())?;
            Ok(duration)
        }
        [minutes, seconds] => {
            let minutes = minutes.parse::<f64>().map_err(|_| Oopsie::BadDuration.to_string())?;
            let seconds = seconds.parse::<f64>().map_err(|_| Oopsie::BadDuration.to_string())?;
            let duration = Duration::try_from_secs_f64(seconds + minutes * 60.)
                .map_err(|_| Oopsie::BadDuration.to_string())?;
            Ok(duration)
        }
        [hours, minutes, seconds] => {
            let hours = hours.parse::<f64>().map_err(|_| Oopsie::BadDuration.to_string())?;
            let minutes = minutes.parse::<f64>().map_err(|_| Oopsie::BadDuration.to_string())?;
            let seconds = seconds.parse::<f64>().map_err(|_| Oopsie::BadDuration.to_string())?;
            let duration = Duration::try_from_secs_f64(seconds + minutes * 60. + hours * 60. * 60.)
                .map_err(|_| Oopsie::BadDuration.to_string())?;
            Ok(duration)
        }
        _ => unreachable!("str::splitn(3, _) returned more than 3 elements, that's illegal"),
    }
}

#[derive(Debug)]
enum Oopsie {
    BadBounds,
    BadDuration,
    /// The best of Jupiter's moons by far; screw you, Ganymede, nobody likes
    /// you
    Io,
    Mp4Stuff,
}

impl Display for Oopsie {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Oopsie::BadBounds => f.write_str("bad cut bounds; do better"),
            Oopsie::BadDuration => f.write_str("bad duration"),
            Oopsie::Io => f.write_str("failed to either read, or write stuff"),
            Oopsie::Mp4Stuff => f.write_str("parsing media files is hard, ok?? get off my back"),
        }
    }
}

impl Error for Oopsie {}

impl From<std::io::Error> for Oopsie {
    fn from(_: std::io::Error) -> Self { Oopsie::Io }
}

impl From<Dangit> for Oopsie {
    fn from(_: Dangit) -> Self { Oopsie::Mp4Stuff }
}

fn do_the_thing(args: Args) -> Result<(), Oopsie> {
    let mut input = File::open(&args.input_file)?;

    let meta = Meta::parse(&mut input)?;
    assert_eq!(meta.magic, 0x00000020, "unexpected magic bytes");

    let output = File::create(&args.output_file)?;

    Ok(())
}
